package globallogic.com.ejercicio.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * Created by Ignacio Saslavsky on 17/9/16.
 * correonano@gmail.com
 */
public class Utils {

    public static String URL = "http://r5---sn-p5qlsnsk.googlevideo.com/videoplayback?source=youtube&signature=D75D12431E1C1A933E0175AA830463636D9A2E31.D0BF6AA6C25439E5D8F040E11225B0E0A41A7818&mime=video%2Fmp4&nh=IgpwcjAzLmlhZDA3KgkxMjcuMC4wLjE&ipbits=0&initcwndbps=3666250&dur=1527.687&lmt=1417192181615650&key=yt6&sparams=dur%2Cei%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cnh%2Cpl%2Cratebypass%2Csource%2Cupn%2Cexpire&ip=159.253.144.86&id=o-AGe5KOowBBRo24-RTmQ5VTqeQSyiBF08NsyIpvIy8F_M&ratebypass=yes&mm=31&mn=sn-p5qlsnsk&itag=18&expire=1474323219&upn=bYzub86CNd4&ei=sw7gV97yAc_gcOTNkLAH&ms=au&mt=1474300572&mv=m&pl=24&title=Barney+%26+Friends%3A+Good+Job%21+%28Season+6%2C+Episode+14%29";

    public static boolean isNetworkAvailable(Context ctx) {
        ConnectivityManager connectivityManager = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String getURL(String html) {
        Document doc = Jsoup.parse(html);
        return doc.getElementsByTag("a").get(42).attr("abs:href");
    }
}
