package globallogic.com.ejercicio.util;

import java.util.ArrayList;
import java.util.List;

import globallogic.com.ejercicio.datastore.DataStore;
import globallogic.com.ejercicio.model.VideoItem;

/**
 * Created by Ignacio Saslavsky on 17/9/16.
 * correonano@gmail.com
 */
public class CacheManager {

    public static List<VideoItem> getVideoItemsFromChache() {
        return new ArrayList<>(DataStore.getInstance().getAll(VideoItem.class));
    }

    public static void updateVideoItemsOnCache(List<VideoItem> videoItems) {
        DataStore.getInstance().deleteAll(VideoItem.class);
        DataStore.getInstance().putAll(videoItems, VideoItem.class);
    }
}
