package globallogic.com.ejercicio.util;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v7.app.NotificationCompat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import globallogic.com.ejercicio.R;
import okhttp3.ResponseBody;

/**
 * Created by Ignacio Saslavsky on 19/9/16.
 * correonano@gmail.com
 */
public class DownloaderManager {

    private static int COUNT = 0;
    private int id;
    NotificationManager mNotifyManager;
    NotificationCompat.Builder mBuilder;

    public DownloaderManager(Context ctx) {
        id = COUNT++;
        mNotifyManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(ctx);
    }

    public boolean writeResponseBodyToDisk(Context ctx, ResponseBody body, String fileName) {

        createNotification(fileName);

        try {
            File file = new File(ctx.getExternalFilesDir(null) + File.separator + fileName + "_" + System.currentTimeMillis() +".mp4");

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(file);
                long startTime = System.currentTimeMillis();
                int timeCount = 1;

                while (true) {
                    int read = inputStream.read(fileReader);
                    if (read == -1) {
                        break;
                    }
                    outputStream.write(fileReader, 0, read);
                    fileSizeDownloaded += read;

                    int progress = (int) ((fileSizeDownloaded * 100) / fileSize);
                    long currentTime = System.currentTimeMillis() - startTime;

                    if (currentTime > 1000 * timeCount) {

                        updateNotification(progress);
                        timeCount++;
                    }
                }

                outputStream.flush();
                endNotification();
                return true;

            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    public void createNotification(String fileName) {
        id++;
        mBuilder.setContentTitle("Video Download")
                .setContentText("Download in progress:" + fileName)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setOngoing(true);

        mBuilder.setProgress(100, 0, false);
        mNotifyManager.notify(id, mBuilder.build());
    }

    public void updateNotification(int progress) {
        mBuilder.setProgress(100, progress, false);
        mNotifyManager.notify(id, mBuilder.build());
    }

    public void endNotification() {
        mBuilder.setContentText("Download complete")
                .setOngoing(false)
                .setProgress(0,0,false);
        mNotifyManager.notify(id, mBuilder.build());
    }
}
