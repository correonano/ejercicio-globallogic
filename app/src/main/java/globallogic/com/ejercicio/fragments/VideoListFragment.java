package globallogic.com.ejercicio.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.List;

import globallogic.com.ejercicio.R;
import globallogic.com.ejercicio.api.GlobalLogicApi;
import globallogic.com.ejercicio.datastore.DataStore;
import globallogic.com.ejercicio.model.VideoItem;
import globallogic.com.ejercicio.services.VideoDownloaderService;
import globallogic.com.ejercicio.util.CacheManager;
import globallogic.com.ejercicio.util.Utils;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Ignacio Saslavsky on 16/9/16.
 * correonano@gmail.com
 */
@ContentView(R.layout.video_list_fragment)
public class VideoListFragment extends BaseFragment {

    @InjectView(R.id.video_list)
    private RecyclerView recyclerView;
    private VideoListAdapter adapter;

    @Inject
    GlobalLogicApi mApi;

    boolean mBound = false;

    public VideoDownloaderService mService;

    public static VideoListFragment newInstance() {
        VideoListFragment instance = new VideoListFragment();
        return instance;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        bindService();
        updateFeeds();

    }

    private void bindService() {
        Intent intent = new Intent(getActivity(), VideoDownloaderService.class);
        getActivity().startService(intent);
        getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    private void updateFeeds() {
        if(Utils.isNetworkAvailable(getActivity())) {
            showLoading();
            mApi.feeds()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(videoItems -> {
                        stopLoading();
                        adapter = new VideoListAdapter(videoItems);
                        recyclerView.setAdapter(adapter);
                        CacheManager.updateVideoItemsOnCache(videoItems);
                    }, error -> {
                        stopLoading();
                    });

        } else {
            List<VideoItem> videoItems = new ArrayList<>(DataStore.getInstance().getAll(VideoItem.class));
            if(videoItems.size() > 0) {
                adapter = new VideoListAdapter(CacheManager.getVideoItemsFromChache());
                recyclerView.setAdapter(adapter);
            }
        }
    }

    private void downloadVideo(String name, String id) {
        if(Utils.isNetworkAvailable(getActivity())) {
            Toast.makeText(getActivity(), "Download video nº " + id, Toast.LENGTH_SHORT).show();
            mService.downloadVideo(name + id);
        } else {
            Toast.makeText(getActivity(), "No hay conexión, intente mas tarde", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        finish();
        if (mBound) {
            getActivity().unbindService(mConnection);
            mBound = false;
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            VideoDownloaderService.LocalBinder binder = (VideoDownloaderService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };


    public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.ViewHolder> {

        private List<VideoItem> dataset;

        public class ViewHolder extends RecyclerView.ViewHolder {

            public final TextView title;
            public final TextView description;

            public ViewHolder(View view) {
                super(view);
                title = (TextView) view.findViewById(R.id.title);
                description = (TextView) view.findViewById(R.id.description);

            }

        }

        public VideoListAdapter(List<VideoItem> dataset) {
            this.dataset = dataset;
        }

        @Override
        public VideoListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_list_row, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final VideoItem videoItem = dataset.get(position);
            holder.title.setText(videoItem.title);
            holder.description.setText(videoItem.description);

            holder.itemView.setOnClickListener(v -> {
                downloadVideo(videoItem.title, videoItem.id);
            });
        }

        @Override
        public int getItemCount() {
            return dataset.size();
        }
    }

}
