package globallogic.com.ejercicio.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.inject.Inject;

import globallogic.com.ejercicio.R;
import globallogic.com.ejercicio.api.GlobalLogicApi;
import globallogic.com.ejercicio.datastore.DataStore;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Ignacio Saslavsky on 15/9/16.
 * correonano@gmail.com
 */
@ContentView(R.layout.login_fragment)
public class LoginFragment extends BaseFragment {

    @InjectView(R.id.username)
    EditText usernameText;
    @InjectView(R.id.password)
    EditText passwordText;
    @InjectView(R.id.login)
    Button entrar;
    @InjectView(R.id.completame)
    CheckBox completame;

    @Inject
    GlobalLogicApi mApi;

    public static LoginFragment newInstance() {
        LoginFragment instance = new LoginFragment();

        return instance;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        entrar.setOnClickListener(v -> {
            if (isValid()) {
                showLoading();
                mApi.login(usernameText.getText().toString().trim(), passwordText.getText().toString().trim())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(user -> {
                            stopLoading();
                            user.timestamp = System.currentTimeMillis();
                            DataStore.getInstance().put(user);
                            start(VideoListFragment.newInstance(), false);
                        }, error -> {
                            stopLoading();
                            error.printStackTrace();
                            Toast.makeText(getActivity(), "Ingrese nombre de usuario/contraseña válidos", Toast.LENGTH_LONG).show();
                        });
            }
        });

        completame.setOnCheckedChangeListener((compoundButton, checked) -> {
            if(checked) {
                usernameText.setText("globallogic_user");
                passwordText.setText("global1234");
            }
            else {
                usernameText.setText("");
                passwordText.setText("");
            }
        });
    }

    private boolean isValid() {
       boolean valid = true;

       if(TextUtils.isEmpty(usernameText.getText())) {
            usernameText.setError(getString(R.string.error_username));
            valid = false;
        }
        if(TextUtils.isEmpty(passwordText.getText())) {
            passwordText.setError(getString(R.string.error_password));
            valid = false;
        }
        return valid;
    }
}
