package globallogic.com.ejercicio.api;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.List;

import globallogic.com.ejercicio.Ejercicio;
import globallogic.com.ejercicio.model.User;
import globallogic.com.ejercicio.model.VideoItem;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import roboguice.RoboGuice;
import rx.Observable;

/**
 * Created by Ignacio Saslavsky on 15/9/16.
 * correonano@gmail.com
 */
@Singleton
public class GlobalLogicApi {


    private Api mApi;

    @Inject
    public GlobalLogicApi() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://private-8f7bb-androidarea.apiary-mock.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        mApi = retrofit.create(Api.class);
    }

    public static GlobalLogicApi getInstance() {
        return RoboGuice.getInjector(Ejercicio.getInstance()).getInstance(GlobalLogicApi.class);
    }


    public Observable<User> login(String username, String password) {
        return mApi.login(username, password);
    }

    public Observable<List<VideoItem>> feeds() {
        return mApi.feeds();
    }

    public Observable<Response<ResponseBody>> downloadVideo(String url) {
        return mApi.downloadVideo(url);
    }


    public Observable<Response<ResponseBody>> getSite(String url) {
        return mApi.getsite(url);
    }
}
