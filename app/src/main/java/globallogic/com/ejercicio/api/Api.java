package globallogic.com.ejercicio.api;

import java.util.List;

import globallogic.com.ejercicio.model.User;
import globallogic.com.ejercicio.model.VideoItem;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Streaming;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by Ignacio Saslavsky on 15/9/16.
 * correonano@gmail.com
 */
public interface Api {

    @GET("login/{username}/{password}")
    Observable<User> login(@Path("username") String username, @Path("password") String password);

    @GET("feeds")
    Observable<List<VideoItem>> feeds();

    @Streaming
    @GET
    Observable<Response<ResponseBody>>  downloadVideo(@Url String url);

    @GET
    Observable<Response<ResponseBody>> getsite(@Url String url);
}
