package globallogic.com.ejercicio.model;

import java.io.Serializable;

/**
 * Created by Ignacio Saslavsky on 16/9/16.
 * correonano@gmail.com
 */
public class User implements Serializable {

    public String name;
    public long timestamp;

    public boolean isValid() {
        return System.currentTimeMillis() - timestamp < 300000;
    }

}
