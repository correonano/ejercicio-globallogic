package globallogic.com.ejercicio.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Ignacio Saslavsky on 17/9/16.
 * correonano@gmail.com
 */
public class VideoItem implements Serializable {

    public String id;
    public String title;
    public String description;
    public String video;
    public Date createdAt;
}
