package globallogic.com.ejercicio.services;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.MainThread;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.inject.Inject;

import java.io.IOException;

import globallogic.com.ejercicio.api.GlobalLogicApi;
import globallogic.com.ejercicio.util.DownloaderManager;
import globallogic.com.ejercicio.util.Utils;
import roboguice.service.RoboService;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Ignacio Saslavsky on 18/9/16.
 * correonano@gmail.com
 */
public class VideoDownloaderService extends RoboService {

    @Inject
    GlobalLogicApi mApi;

    private final IBinder mBinder = new LocalBinder();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    public void downloadVideo(String filename) {

        mApi.getSite("http://keepvid.com/?url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DninU5rEbA0Q")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(response -> {
                    try {
                        downloadVideo(filename, Utils.getURL(response.body().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

    }

    private void downloadVideo(String filename, String url) {
        mApi.downloadVideo(url)
            .subscribeOn(Schedulers.io())
            .subscribe(responseBody -> {
                DownloaderManager manager = new DownloaderManager(VideoDownloaderService.this);
                manager.writeResponseBodyToDisk(this, responseBody.body(), filename);

            }, error -> {
                error.printStackTrace();
                Log.d("error", "error...");
            });
    }

    public class LocalBinder extends Binder {
        public VideoDownloaderService getService() {
            return VideoDownloaderService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

}