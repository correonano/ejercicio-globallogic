package globallogic.com.ejercicio;

import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;

import roboguice.RoboGuice;

/**
 * Created by Ignacio Saslavsky on 23/7/16.
 * correonano@gmail.com
 */
public class Ejercicio extends Application {

    private static Ejercicio sInstance;

    public static Ejercicio getInstance() {
        return sInstance;
    }

    private static final String PREF = "pref";

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        RoboGuice.setUseAnnotationDatabases(false);
        RoboGuice.injectMembers(this, this);

    }

}
