package globallogic.com.ejercicio.activities;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import globallogic.com.ejercicio.R;
import globallogic.com.ejercicio.fragments.BaseFragment;
import roboguice.activity.RoboActionBarActivity;

/**
 * Created by Ignacio Saslavsky on 15/9/16.
 * correonano@gmail.com
 */
public class BaseActivity extends RoboActionBarActivity {

    private ProgressDialog progressDialog;

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void start(Fragment fragment, boolean addToBackStack) {
        start(fragment, addToBackStack, R.id.main_content);
    }

    public void start(Fragment fragment, boolean addToBackStack, int containerViewId) {
        FragmentTransaction tx = getSupportFragmentManager().beginTransaction().replace(containerViewId, fragment);
        if (addToBackStack) {
            tx.addToBackStack(fragment.getClass().getSimpleName());
        }
        tx.commit();
    }

    public <T extends BaseFragment> void start(T fragment) {
        start(fragment, true);
    }

    public boolean popBackStack() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            return true;
        }
        return false;
    }

    public void stopLoading() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public void showLoading(boolean cancelable) {
        showLoading(cancelable, R.string.loading);
    }

    public void showLoading(boolean cancelable, int message) {
        showLoading(cancelable, getString(message));
    }

    public void showLoading(boolean cancelable, String message) {
        if (progressDialog == null) {
            runOnUiThread(() -> {
                progressDialog = ProgressDialog.show(this, null, message, true, cancelable);
                progressDialog.setOnCancelListener(dialog -> progressDialog = null);
                progressDialog.setOnDismissListener(dialog -> progressDialog = null);
            });
        } else {
            Log.d("baseActivity", "Already showing progress dialog.");
        }
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

}

