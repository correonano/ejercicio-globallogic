package globallogic.com.ejercicio.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import java.util.Set;

import globallogic.com.ejercicio.R;
import globallogic.com.ejercicio.datastore.DataStore;
import globallogic.com.ejercicio.fragments.LoginFragment;
import globallogic.com.ejercicio.fragments.VideoListFragment;
import globallogic.com.ejercicio.model.User;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);

        Set<User> users = DataStore.getInstance().getAll(User.class);
        if (users.size() > 0 && ((User)users.toArray()[0]).isValid()) {
            start(VideoListFragment.newInstance(), false);
        } else {
            start(LoginFragment.newInstance(), false);
        }
    }
}
